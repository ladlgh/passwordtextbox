﻿using System;
using System.Windows.Forms;

namespace PasswordTextBoxDemo
{
    public partial class frm_Demo : Form
    {
        public frm_Demo()
        {
            InitializeComponent();
        }

        private void frm_Demo_Shown(object sender, EventArgs e)
        {
            ptb_Password11.Focus();
        }

        private void btn_Login_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this,
                            "Username: " + tb_Usename.Text + "\n" +
                            "Password: " + ptb_Password41.HiddenText,
                            "INFO...",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }
    }
}
