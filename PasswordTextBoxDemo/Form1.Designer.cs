﻿namespace PasswordTextBoxDemo
{
    partial class frm_Demo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gb_None = new System.Windows.Forms.GroupBox();
            this.ptb_Password11 = new PasswordTextBox.PasswordTextBox();
            this.ptb_Password13 = new PasswordTextBox.PasswordTextBox();
            this.ptb_Password12 = new PasswordTextBox.PasswordTextBox();
            this.gb_FixedSingle = new System.Windows.Forms.GroupBox();
            this.ptb_Password21 = new PasswordTextBox.PasswordTextBox();
            this.ptb_Password23 = new PasswordTextBox.PasswordTextBox();
            this.ptb_Password22 = new PasswordTextBox.PasswordTextBox();
            this.gb_Fixed3D = new System.Windows.Forms.GroupBox();
            this.ptb_Password31 = new PasswordTextBox.PasswordTextBox();
            this.ptb_Password33 = new PasswordTextBox.PasswordTextBox();
            this.ptb_Password32 = new PasswordTextBox.PasswordTextBox();
            this.gb_Login = new System.Windows.Forms.GroupBox();
            this.btn_Login = new System.Windows.Forms.Button();
            this.tb_Usename = new System.Windows.Forms.TextBox();
            this.ptb_Password41 = new PasswordTextBox.PasswordTextBox();
            this.gb_None.SuspendLayout();
            this.gb_FixedSingle.SuspendLayout();
            this.gb_Fixed3D.SuspendLayout();
            this.gb_Login.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_None
            // 
            this.gb_None.BackColor = System.Drawing.Color.LightGray;
            this.gb_None.Controls.Add(this.ptb_Password11);
            this.gb_None.Controls.Add(this.ptb_Password13);
            this.gb_None.Controls.Add(this.ptb_Password12);
            this.gb_None.Dock = System.Windows.Forms.DockStyle.Top;
            this.gb_None.Location = new System.Drawing.Point(0, 0);
            this.gb_None.Margin = new System.Windows.Forms.Padding(0);
            this.gb_None.Name = "gb_None";
            this.gb_None.Padding = new System.Windows.Forms.Padding(0);
            this.gb_None.Size = new System.Drawing.Size(180, 100);
            this.gb_None.TabIndex = 6;
            this.gb_None.TabStop = false;
            this.gb_None.Text = "BorderStyle: None";
            // 
            // ptb_Password11
            // 
            this.ptb_Password11.AllowedCharsRegex = "0-9a-z .,:;_\\-!§$% &/()=?#+*";
            this.ptb_Password11.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ptb_Password11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ptb_Password11.HintColor = System.Drawing.SystemColors.GrayText;
            this.ptb_Password11.HintText = "Password";
            this.ptb_Password11.Location = new System.Drawing.Point(5, 20);
            this.ptb_Password11.Margin = new System.Windows.Forms.Padding(0);
            this.ptb_Password11.MaxLength = 1000;
            this.ptb_Password11.Name = "ptb_Password11";
            this.ptb_Password11.PasswordCharDelay = 500;
            this.ptb_Password11.ShowPasswordEye = false;
            this.ptb_Password11.Size = new System.Drawing.Size(170, 13);
            this.ptb_Password11.TabIndex = 5;
            // 
            // ptb_Password13
            // 
            this.ptb_Password13.AllowedCharsRegex = "0-9a-z .,:;_\\-!§$% &/()=?#+*";
            this.ptb_Password13.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ptb_Password13.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ptb_Password13.HintColor = System.Drawing.SystemColors.GrayText;
            this.ptb_Password13.HintText = "Password";
            this.ptb_Password13.Location = new System.Drawing.Point(5, 70);
            this.ptb_Password13.Margin = new System.Windows.Forms.Padding(0);
            this.ptb_Password13.MaxLength = 1000;
            this.ptb_Password13.Name = "ptb_Password13";
            this.ptb_Password13.PasswordCharDelay = 500;
            this.ptb_Password13.ShowPasswordEye = true;
            this.ptb_Password13.Size = new System.Drawing.Size(170, 13);
            this.ptb_Password13.TabIndex = 5;
            // 
            // ptb_Password12
            // 
            this.ptb_Password12.AllowedCharsRegex = "0-9a-z .,:;_\\-!§$% &/()=?#+*";
            this.ptb_Password12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ptb_Password12.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ptb_Password12.HintColor = System.Drawing.SystemColors.GrayText;
            this.ptb_Password12.HintText = "Password";
            this.ptb_Password12.Location = new System.Drawing.Point(5, 45);
            this.ptb_Password12.Margin = new System.Windows.Forms.Padding(0);
            this.ptb_Password12.MaxLength = 1000;
            this.ptb_Password12.Name = "ptb_Password12";
            this.ptb_Password12.PasswordCharDelay = 500;
            this.ptb_Password12.ShowPasswordEye = true;
            this.ptb_Password12.Size = new System.Drawing.Size(170, 13);
            this.ptb_Password12.TabIndex = 5;
            // 
            // gb_FixedSingle
            // 
            this.gb_FixedSingle.BackColor = System.Drawing.Color.LightGray;
            this.gb_FixedSingle.Controls.Add(this.ptb_Password21);
            this.gb_FixedSingle.Controls.Add(this.ptb_Password23);
            this.gb_FixedSingle.Controls.Add(this.ptb_Password22);
            this.gb_FixedSingle.Dock = System.Windows.Forms.DockStyle.Top;
            this.gb_FixedSingle.Location = new System.Drawing.Point(0, 100);
            this.gb_FixedSingle.Margin = new System.Windows.Forms.Padding(0);
            this.gb_FixedSingle.Name = "gb_FixedSingle";
            this.gb_FixedSingle.Padding = new System.Windows.Forms.Padding(0);
            this.gb_FixedSingle.Size = new System.Drawing.Size(180, 100);
            this.gb_FixedSingle.TabIndex = 6;
            this.gb_FixedSingle.TabStop = false;
            this.gb_FixedSingle.Text = "BorderStyle: FixedSingle";
            // 
            // ptb_Password21
            // 
            this.ptb_Password21.AllowedCharsRegex = "0-9a-z .,:;_\\-!§$% &/()=?#+*";
            this.ptb_Password21.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ptb_Password21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptb_Password21.HintColor = System.Drawing.SystemColors.GrayText;
            this.ptb_Password21.HintText = "Password";
            this.ptb_Password21.Location = new System.Drawing.Point(5, 20);
            this.ptb_Password21.Margin = new System.Windows.Forms.Padding(0);
            this.ptb_Password21.MaxLength = 1000;
            this.ptb_Password21.Name = "ptb_Password21";
            this.ptb_Password21.PasswordCharDelay = 500;
            this.ptb_Password21.ShowPasswordEye = false;
            this.ptb_Password21.Size = new System.Drawing.Size(170, 20);
            this.ptb_Password21.TabIndex = 5;
            // 
            // ptb_Password23
            // 
            this.ptb_Password23.AllowedCharsRegex = "0-9a-z .,:;_\\-!§$% &/()=?#+*";
            this.ptb_Password23.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ptb_Password23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptb_Password23.HintColor = System.Drawing.SystemColors.GrayText;
            this.ptb_Password23.HintText = "Password";
            this.ptb_Password23.Location = new System.Drawing.Point(5, 70);
            this.ptb_Password23.Margin = new System.Windows.Forms.Padding(0);
            this.ptb_Password23.MaxLength = 1000;
            this.ptb_Password23.Name = "ptb_Password23";
            this.ptb_Password23.PasswordCharDelay = 500;
            this.ptb_Password23.ShowPasswordEye = true;
            this.ptb_Password23.Size = new System.Drawing.Size(170, 20);
            this.ptb_Password23.TabIndex = 5;
            // 
            // ptb_Password22
            // 
            this.ptb_Password22.AllowedCharsRegex = "0-9a-z .,:;_\\-!§$% &/()=?#+*";
            this.ptb_Password22.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ptb_Password22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptb_Password22.HintColor = System.Drawing.SystemColors.GrayText;
            this.ptb_Password22.HintText = "Password";
            this.ptb_Password22.Location = new System.Drawing.Point(5, 45);
            this.ptb_Password22.Margin = new System.Windows.Forms.Padding(0);
            this.ptb_Password22.MaxLength = 1000;
            this.ptb_Password22.Name = "ptb_Password22";
            this.ptb_Password22.PasswordCharDelay = 500;
            this.ptb_Password22.ShowPasswordEye = true;
            this.ptb_Password22.Size = new System.Drawing.Size(170, 20);
            this.ptb_Password22.TabIndex = 5;
            // 
            // gb_Fixed3D
            // 
            this.gb_Fixed3D.BackColor = System.Drawing.Color.LightGray;
            this.gb_Fixed3D.Controls.Add(this.ptb_Password31);
            this.gb_Fixed3D.Controls.Add(this.ptb_Password33);
            this.gb_Fixed3D.Controls.Add(this.ptb_Password32);
            this.gb_Fixed3D.Dock = System.Windows.Forms.DockStyle.Top;
            this.gb_Fixed3D.Location = new System.Drawing.Point(0, 200);
            this.gb_Fixed3D.Margin = new System.Windows.Forms.Padding(0);
            this.gb_Fixed3D.Name = "gb_Fixed3D";
            this.gb_Fixed3D.Padding = new System.Windows.Forms.Padding(0);
            this.gb_Fixed3D.Size = new System.Drawing.Size(180, 100);
            this.gb_Fixed3D.TabIndex = 6;
            this.gb_Fixed3D.TabStop = false;
            this.gb_Fixed3D.Text = "BorderStyle: Fixed3D";
            // 
            // ptb_Password31
            // 
            this.ptb_Password31.AllowedCharsRegex = "0-9a-z .,:;_\\-!§$% &/()=?#+*";
            this.ptb_Password31.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ptb_Password31.HintColor = System.Drawing.SystemColors.GrayText;
            this.ptb_Password31.HintText = "Password";
            this.ptb_Password31.Location = new System.Drawing.Point(5, 20);
            this.ptb_Password31.Margin = new System.Windows.Forms.Padding(0);
            this.ptb_Password31.MaxLength = 1000;
            this.ptb_Password31.Name = "ptb_Password31";
            this.ptb_Password31.PasswordCharDelay = 500;
            this.ptb_Password31.ShowPasswordEye = false;
            this.ptb_Password31.Size = new System.Drawing.Size(170, 20);
            this.ptb_Password31.TabIndex = 5;
            // 
            // ptb_Password33
            // 
            this.ptb_Password33.AllowedCharsRegex = "0-9a-z .,:;_\\-!§$% &/()=?#+*";
            this.ptb_Password33.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ptb_Password33.HintColor = System.Drawing.SystemColors.GrayText;
            this.ptb_Password33.HintText = "Password";
            this.ptb_Password33.Location = new System.Drawing.Point(5, 70);
            this.ptb_Password33.Margin = new System.Windows.Forms.Padding(0);
            this.ptb_Password33.MaxLength = 1000;
            this.ptb_Password33.Name = "ptb_Password33";
            this.ptb_Password33.PasswordCharDelay = 500;
            this.ptb_Password33.ShowPasswordEye = true;
            this.ptb_Password33.Size = new System.Drawing.Size(170, 20);
            this.ptb_Password33.TabIndex = 5;
            // 
            // ptb_Password32
            // 
            this.ptb_Password32.AllowedCharsRegex = "0-9a-z .,:;_\\-!§$% &/()=?#+*";
            this.ptb_Password32.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ptb_Password32.HintColor = System.Drawing.SystemColors.GrayText;
            this.ptb_Password32.HintText = "Password";
            this.ptb_Password32.Location = new System.Drawing.Point(5, 45);
            this.ptb_Password32.Margin = new System.Windows.Forms.Padding(0);
            this.ptb_Password32.MaxLength = 1000;
            this.ptb_Password32.Name = "ptb_Password32";
            this.ptb_Password32.PasswordCharDelay = 500;
            this.ptb_Password32.ShowPasswordEye = true;
            this.ptb_Password32.Size = new System.Drawing.Size(170, 20);
            this.ptb_Password32.TabIndex = 5;
            // 
            // gb_Login
            // 
            this.gb_Login.BackColor = System.Drawing.Color.LightGray;
            this.gb_Login.Controls.Add(this.btn_Login);
            this.gb_Login.Controls.Add(this.tb_Usename);
            this.gb_Login.Controls.Add(this.ptb_Password41);
            this.gb_Login.Dock = System.Windows.Forms.DockStyle.Top;
            this.gb_Login.Location = new System.Drawing.Point(0, 300);
            this.gb_Login.Margin = new System.Windows.Forms.Padding(0);
            this.gb_Login.Name = "gb_Login";
            this.gb_Login.Padding = new System.Windows.Forms.Padding(0);
            this.gb_Login.Size = new System.Drawing.Size(180, 100);
            this.gb_Login.TabIndex = 6;
            this.gb_Login.TabStop = false;
            this.gb_Login.Text = "Login";
            // 
            // btn_Login
            // 
            this.btn_Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Login.Location = new System.Drawing.Point(100, 70);
            this.btn_Login.Margin = new System.Windows.Forms.Padding(0);
            this.btn_Login.Name = "btn_Login";
            this.btn_Login.Size = new System.Drawing.Size(75, 23);
            this.btn_Login.TabIndex = 7;
            this.btn_Login.Text = "Login";
            this.btn_Login.UseVisualStyleBackColor = true;
            this.btn_Login.Click += new System.EventHandler(this.btn_Login_Click);
            // 
            // tb_Usename
            // 
            this.tb_Usename.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_Usename.Location = new System.Drawing.Point(5, 20);
            this.tb_Usename.Margin = new System.Windows.Forms.Padding(0);
            this.tb_Usename.Name = "tb_Usename";
            this.tb_Usename.Size = new System.Drawing.Size(170, 20);
            this.tb_Usename.TabIndex = 6;
            // 
            // ptb_Password41
            // 
            this.ptb_Password41.AllowedCharsRegex = "0-9a-z .,:;_\\-!§$% &/()=?#+*";
            this.ptb_Password41.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ptb_Password41.HintColor = System.Drawing.SystemColors.GrayText;
            this.ptb_Password41.HintText = "Password";
            this.ptb_Password41.Location = new System.Drawing.Point(5, 45);
            this.ptb_Password41.Margin = new System.Windows.Forms.Padding(0);
            this.ptb_Password41.MaxLength = 1000;
            this.ptb_Password41.Name = "ptb_Password41";
            this.ptb_Password41.PasswordCharDelay = 500;
            this.ptb_Password41.ShowPasswordEye = true;
            this.ptb_Password41.Size = new System.Drawing.Size(170, 20);
            this.ptb_Password41.TabIndex = 5;
            // 
            // frm_Demo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(180, 402);
            this.Controls.Add(this.gb_Login);
            this.Controls.Add(this.gb_Fixed3D);
            this.Controls.Add(this.gb_FixedSingle);
            this.Controls.Add(this.gb_None);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frm_Demo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PasswordTextBox Demo";
            this.Shown += new System.EventHandler(this.frm_Demo_Shown);
            this.gb_None.ResumeLayout(false);
            this.gb_None.PerformLayout();
            this.gb_FixedSingle.ResumeLayout(false);
            this.gb_FixedSingle.PerformLayout();
            this.gb_Fixed3D.ResumeLayout(false);
            this.gb_Fixed3D.PerformLayout();
            this.gb_Login.ResumeLayout(false);
            this.gb_Login.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private PasswordTextBox.PasswordTextBox ptb_Password11;
        private PasswordTextBox.PasswordTextBox ptb_Password12;
        private System.Windows.Forms.GroupBox gb_None;
        private System.Windows.Forms.GroupBox gb_FixedSingle;
        private PasswordTextBox.PasswordTextBox ptb_Password21;
        private PasswordTextBox.PasswordTextBox ptb_Password22;
        private System.Windows.Forms.GroupBox gb_Fixed3D;
        private PasswordTextBox.PasswordTextBox ptb_Password31;
        private PasswordTextBox.PasswordTextBox ptb_Password32;
        private PasswordTextBox.PasswordTextBox ptb_Password13;
        private PasswordTextBox.PasswordTextBox ptb_Password23;
        private PasswordTextBox.PasswordTextBox ptb_Password33;
        private System.Windows.Forms.GroupBox gb_Login;
        private PasswordTextBox.PasswordTextBox ptb_Password41;
        private System.Windows.Forms.Button btn_Login;
        private System.Windows.Forms.TextBox tb_Usename;
    }
}

