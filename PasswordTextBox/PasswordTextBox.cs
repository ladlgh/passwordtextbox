﻿/*
*  ----- FORK OF ------------------------------
*  
*  Title     : CustomPasswordTextboxWithPreview
*  Author(s) : SrivatsaHaridas
*  Version   : 1.4.0.0
*  GitHub    : https://github.com/srivatsahg/password-with-preview
*  
*  ----- AND ------------------------------
*  
*  Title     : TextBoxWithPasswordEyeIcon
*  Author(s) : RezaAghaei
*  Version   : 1.0.0.0
*  GitHub    : https://github.com/r-aghaei/TextBoxWithShowPasswordEyeIcon
*  
*  ----- AND ------------------------------
*  
*  Title     : TextBoxWatermark
*  Author(s) : RezaAghaei
*  Version   : 1.0.0.0
*  GitHub    : https://github.com/r-aghaei/TextBoxWatermark
*/

using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Timers;
using System.Windows.Forms;

namespace PasswordTextBox
{
    public partial class PasswordTextBox : TextBox
    {
        public bool IsHidden = true;
        
        private int CaretPosition = 0;
        private bool CanEdit = true;
        private char[] PWD_ARRAY     = new char[0];
        private static char PWD_CHAR = (new TextBox() { UseSystemPasswordChar = true }).PasswordChar;
        private static char TXT_CHAR = '\0';
        
        [DllImport("user32.dll")]
        static extern IntPtr SendMessage(IntPtr hwnd, int msg, int wParam, int lParam);
        const int EM_SETMARGINS  = 0xd3;
        const int EC_RIGHTMARGIN = 2;

        private static int _PasswordCharDelay = 500;
        [Category("Behavior")]
        [DisplayName("PasswordCharDelay")]
        public int PasswordCharDelay
        {
            get { return _PasswordCharDelay; }
            set
            {
                _PasswordCharDelay = (value > 0) ? value : 1;
                Timer.Interval     = (value > 0) ? value : 1;
            }
        }
        
        private bool _ShowPasswordEye = false;
        [Category("Behavior")]
        [DisplayName("ShowPasswordEye")]
        public bool ShowPasswordEye
        {
            get { return _ShowPasswordEye; }
            set
            {
                _ShowPasswordEye = value;
                InitEyePictureBox();
            }
        }

        private string _AllowedCharsRegex = @"0-9a-z .,:;_\-!§$% &/()=?#+*";
        [Category("Behavior")]
        [DisplayName("AllowedCharsRegex")]
        public string AllowedCharsRegex
        {
            get { return _AllowedCharsRegex; }
            set { _AllowedCharsRegex = value; }
        }

        private string _HintText = "Password";
        [Category("Appearance")]
        [DisplayName("HintText")]
        public string HintText
        {
            get { return _HintText; }
            set
            {
                _HintText = value;
                Invalidate();
            }
        }

        private Color _HintColor = SystemColors.GrayText;
        [Category("Appearance")]
        [DisplayName("HintColor")]
        public Color HintColor
        {
            get { return _HintColor; }
            set
            {
                _HintColor = value;
                Invalidate();
            }
        }
        
        [Category("Appearance")]
        [DisplayName("HiddenText")]
        public string HiddenText
        {
            get { return new string(PWD_ARRAY).Trim(TXT_CHAR).Replace(TXT_CHAR.ToString(), ""); }
        }
        
        [Browsable(false)]
        [Bindable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("This property has been deprecated and should no longer be used.", true)]
        public new char Multiline { get; set; } // hide it

        [Browsable(false)]
        [Bindable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("This property has been deprecated and should no longer be used.", true)]
        public new char PasswordChar { get; set; } // hide it

        [Browsable(false)]
        [Bindable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("This property has been deprecated and should no longer be used.", true)]
        public new bool UseSystemPasswordChar { get; set; } // hide it

        [Browsable(false)]
        [Bindable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("This property has been deprecated and should no longer be used.", true)]
        public new char WordWrap { get; set; } // hide it

        public PasswordTextBox() : base()
        {
            InitializeComponent();
            
            // init variables
            Array.Resize(ref PWD_ARRAY, MaxLength);

            // init picturebox
            InitEyePictureBox();
        }
        
        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (m.Msg == 0xf)
            {
                if (!Focused &&
                    string.IsNullOrEmpty(HiddenText) &&
                    !string.IsNullOrEmpty(HintText))
                {
                    using (var graphics = CreateGraphics())
                    {
                        TextRenderer.DrawText
                        (
                            graphics,
                            HintText,
                            Font,
                            ClientRectangle,
                            HintColor,
                            BackColor,
                            TextFormatFlags.Left | TextFormatFlags.VerticalCenter
                        );
                    }
                }
            }
        }
        
        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);

            SetTextMargin((_ShowPasswordEye) ? ClientSize.Width - EyePictureBox.Location.X : 0);
        }

        protected override void OnTextChanged(EventArgs e)
        {
            if (!CanEdit) return;
            
            base.OnTextChanged(e);
            HidePasswordCharacters();
        }
        
        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            CaretPosition = GetCharIndexFromPosition(e.Location);
        }
        
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (e.KeyCode == Keys.Delete)
            {
                CanEdit = false;
                DeleteSelectedCharacters(this, e.KeyCode);
            }
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);

            int selectionStart = SelectionStart;
            int length         = TextLength;
            int selectedChars  = SelectionLength;

            var rgx_char = new Regex(@"[" + _AllowedCharsRegex + "]+", RegexOptions.IgnoreCase);

            CanEdit = false;

            if (selectedChars == length)
            {
                ClearCharBufferPlusTextBox();
            }

            Keys key = (Keys)e.KeyChar;
            
            if (key != Keys.Delete &&
                key != Keys.Back)
            {
                if (rgx_char.IsMatch(e.KeyChar.ToString()))
                {
                    if (TextLength < MaxLength)
                    {
                        PWD_ARRAY = new string(PWD_ARRAY).Insert(selectionStart, e.KeyChar.ToString()).ToCharArray();
                    }
                }
                else
                {
                    e.Handled = true;
                }
            }
            else if (key == Keys.Delete ||
                     key == Keys.Back)
            {
                DeleteSelectedCharacters(this, key);
            }
            
            HidePasswordCharacters();

            CanEdit = true;
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            ResizeEyePictureBox();
        }

        protected override void OnBackColorChanged(EventArgs e)
        {
            base.OnBackColorChanged(e);
            
            EyePictureBox.BackColor = BackColor;
        }
        
        private void InitEyePictureBox()
        {
            EyePictureBox.BackColor = BackColor;
            EyePictureBox.Image     = ImageList.Images[Convert.ToInt32(IsHidden)];
            ResizeEyePictureBox();
            EyePictureBox.Visible   = _ShowPasswordEye;

            SetTextMargin((_ShowPasswordEye) ? ClientSize.Width - EyePictureBox.Location.X : 0);
        }

        private void ResizeEyePictureBox()
        {
            EyePictureBox.Margin = new Padding
            (
                (ClientSize.Width < Width) ? 2 : 4,
                (ClientSize.Height < Height) ? 0 : 1,
                (ClientSize.Width < Width) ? 2 : 4,
                (ClientSize.Height < Height) ? 0 : 1
            );

            var height = ClientSize.Height - EyePictureBox.Margin.Top - EyePictureBox.Margin.Bottom;
            var width = Math.Min(ImageList.Images[Convert.ToInt32(IsHidden)].Size.Width, height); ;

            EyePictureBox.Size     = new Size(width, height);
            EyePictureBox.Location = new Point(ClientSize.Width - EyePictureBox.Width - EyePictureBox.Margin.Right, EyePictureBox.Margin.Top);
        }

        private void OnEyePictureBoxClick(object sender, EventArgs e)
        {
            if (IsHidden)
            {
                Timer.Stop();

                IsHidden = !IsHidden;
                Text     = new string(PWD_ARRAY).Trim(TXT_CHAR).Replace(TXT_CHAR.ToString(), "");
            }
            else
            {
                var s = new StringBuilder(Text);

                if (!string.IsNullOrEmpty(Text))
                {
                    for (int i = 0; i < s.Length; i++)
                    {
                        s[i] = PWD_CHAR;
                    }
                }

                IsHidden = !IsHidden;
                Text     = s.ToString();
            }

            SelectionStart = Text.Length;
            CaretPosition  = SelectionStart;

            ((PictureBox)sender).Image = ImageList.Images[Convert.ToInt32(IsHidden)];
        }

        private void SetTextMargin(int width)
        {
            SendMessage(Handle, EM_SETMARGINS, EC_RIGHTMARGIN, width << 16);
        }
        
        private void HidePasswordCharacters()
        {
            if (!IsHidden) return;

            int index = SelectionStart;

            if (index > 1)
            {
                var sb         = new StringBuilder(Text);
                sb[index - 2]  = PWD_CHAR;
                Text           = sb.ToString();
                SelectionStart = index;
                CaretPosition  = index;
            }

            Timer.Interval = _PasswordCharDelay;
            Timer.Start();
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            Timer.Stop();

            int index = 0;

            this.Invoke(new Action(() =>
            {
                index = SelectionStart;
            }));

                if (index >= 1)
            {
                StringBuilder s = new StringBuilder(Text);
                s[index - 1]    = PWD_CHAR;

                this.Invoke(new Action(() =>
                {
                    Text           = s.ToString();
                    SelectionStart = index;
                    CaretPosition  = index;
                }));
            }
        }
        
        private void DeleteSelectedCharacters(object sender, Keys key)
        {
            int selectionStart = SelectionStart,
                length         = TextLength,
                selectedChars  = SelectionLength;

            if (selectedChars == length)
            {
                ClearCharBufferPlusTextBox();

                return;
            }

            if (selectedChars > 0)
            {
                int i = selectionStart;
                Text.Remove(selectionStart, selectedChars);
                PWD_ARRAY = new string(PWD_ARRAY).Remove(selectionStart, selectedChars).ToCharArray();
            }
            else
            {
                /*
                 * Basically this portion of code is to handle the condition 
                 * when the cursor is placed at the start or in the end 
                 */
                if (selectionStart == 0)
                {
                    /*
                    * Cursor in the beginning, before the first character 
                    * Delete the character only when Del is pressed, No action when Back key is pressed
                    */
                    if (key == Keys.Delete)
                    {
                        PWD_ARRAY = new string(PWD_ARRAY).Remove(selectionStart, 1).ToCharArray();
                    }
                }
                else if (selectionStart > 0 && selectionStart < length)
                {
                    /*
                    * Cursor position anywhere in between 
                    * Backspace and Delete have the same effect
                    */
                    if (key == Keys.Delete)
                    {
                        PWD_ARRAY = new string(PWD_ARRAY).Remove(selectionStart, 1).ToCharArray();
                    }
                    else if (key == Keys.Back)
                    {
                        PWD_ARRAY = new string(PWD_ARRAY).Remove(selectionStart - 1, 1).ToCharArray();
                    }
                }
                else if (selectionStart == length)
                {
                    /*
                    * Cursor at the end, after the last character 
                    * Delete the character only when Back key is pressed, No action when Delete key is pressed
                    */
                    if (key == Keys.Back)
                    {
                        PWD_ARRAY = new string(PWD_ARRAY).Remove(selectionStart - 1, 1).ToCharArray();
                    }
                }
            }

            Select((selectionStart > Text.Length ? Text.Length : selectionStart), 0);

        }

        private void ClearCharBufferPlusTextBox()
        {
            Array.Clear(PWD_ARRAY, 0, PWD_ARRAY.Length);
            Clear();
        }
    }
}
