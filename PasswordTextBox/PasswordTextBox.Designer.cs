﻿namespace PasswordTextBox
{
    partial class PasswordTextBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.EyePictureBox.Image = null;
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PasswordTextBox));
            this.EyePictureBox = new System.Windows.Forms.PictureBox();
            this.ImageList = new System.Windows.Forms.ImageList(this.components);
            this.Timer = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.EyePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Timer)).BeginInit();
            this.SuspendLayout();
            // 
            // EyePictureBox
            // 
            this.EyePictureBox.BackColor = System.Drawing.Color.White;
            this.EyePictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.EyePictureBox.Location = new System.Drawing.Point(0, 0);
            this.EyePictureBox.Margin = new System.Windows.Forms.Padding(0);
            this.EyePictureBox.Name = "EyePictureBox";
            this.EyePictureBox.Size = new System.Drawing.Size(18, 18);
            this.EyePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.EyePictureBox.TabIndex = 0;
            this.EyePictureBox.TabStop = false;
            this.EyePictureBox.Visible = false;
            this.EyePictureBox.Click += new System.EventHandler(this.OnEyePictureBoxClick);
            // 
            // ImageList
            // 
            this.ImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList.ImageStream")));
            this.ImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList.Images.SetKeyName(0, "img_EyeVisible");
            this.ImageList.Images.SetKeyName(1, "img_EyeHidden");
            // 
            // Timer
            // 
            this.Timer.SynchronizingObject = this;
            this.Timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimerElapsed);
            // 
            // PasswordTextBox
            // 
            this.Controls.Add(this.EyePictureBox);
            ((System.ComponentModel.ISupportInitialize)(this.EyePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Timer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox EyePictureBox;
        private System.Windows.Forms.ImageList ImageList;
        private System.Timers.Timer Timer;
    }
}
